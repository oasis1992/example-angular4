import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ExampleComponent} from './components/example/example.component';
import {RouterModule, Routes} from '@angular/router';
import {NavComponent} from './components/nav/nav.component';
import {MenuComponent} from './components/example/menu/menu.component';
import {AccountComponent} from './components/example/account/account.component';
import {TitleComponent} from './components/util/title/title.component';
import {CardComponent} from './components/example/cards/card.component';
import {TitleListComponent} from './components/util/title_list/title_list.component';
import {ItemListComponent} from './components/example/item_list/item.component';
import {PlainComponent} from './components/example/plain/plain.component';

const appRoutes: Routes = [
  {path: '', component: ExampleComponent},
  {path: 'test', component: ExampleComponent}
];

@NgModule({
  declarations: [
    AppComponent,
      ExampleComponent,
      NavComponent,
      MenuComponent,
      AccountComponent,
      TitleComponent,
      CardComponent,
      TitleListComponent,
      ItemListComponent,
      PlainComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
