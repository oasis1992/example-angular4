import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-title-list',
    templateUrl: './title_list.component.html',
    styleUrls: ['./title_list.component.sass']
})

export class TitleListComponent {
    @Input() title: string;
}