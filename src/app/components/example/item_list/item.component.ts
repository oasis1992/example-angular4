import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-item-list',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.sass']
})

export class ItemListComponent {
    @Input() text: string;
    @Input() textTwo: string;
}